<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'product_id',
        'customer_name',
        'address'
    ];

    public static function getUserOrders($id)
    {
        return DB::table('orders')
            ->join('products', 'products.id', '=', 'orders.product_id')
            ->select('title', 'price', 'customer_name', 'address')
            ->where('user_id', $id)
            ->get();
    }

    public static function addOrder($userId, $productId)
    {
        return Order::create(['user_id' => $userId, 'product_id' => $productId]);
    }
}
