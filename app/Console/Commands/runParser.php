<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:parser {category}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
   //     echo $this->argument('category');
        exec('node resources/js/parser.js '.$this->argument('category'));
        echo 'done';
        return 1;
    }
}
