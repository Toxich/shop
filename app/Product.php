<?php

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'id',
        'category_id',
        'title',
    ];

    public static function rowsByDateRange($startDate, $endDate)
    {
        $start = Carbon::parse($startDate);
        $end   = Carbon::parse($endDate);

        return self::whereDate('created_at', '<=', $end->format('Y-m-d'))
            ->whereDate('created_at', '>=', $start->format('Y-m-d'))->get();

    }

    public static function getProduct($id)
    {
        return Product::get()->where('id', $id)->first();
    }
}
