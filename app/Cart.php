<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    protected $table = 'carts';
    protected $fillable = [
        'id',
        'user_id',
        'product_id',
    ];

    public static function getUserCart($id)
    {
        return DB::table('carts')
            ->join('products', 'products.id', '=', 'carts.product_id')
            ->select('title', 'price', 'products.id')
            ->where('user_id', $id)
            ->get();
    }

    public static function addOrDeleteCart($productId, $userId)
    {
        $result = Cart::where('user_id', $userId)->where('product_id', $productId)->first();
        if ($result) {
            Cart::where('user_id', $userId)->where('product_id', $productId)->delete();
            return null;
        } else {
            Cart::create(
                [
                    'user_id' => $userId,
                    'product_id' => $productId,
                ]
            );
            return $productId;
        }
    }
}
