<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public static function addOrDeleteCart(Request $request)
    {
        return Cart::addOrDeleteCart($request->get('prodId'), $request->get('userId'));
    }

    public static function getCart(Request $request)
    {
        return Cart::getUserCart($request->get('id'));
    }
}
