<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public static function getOrders(Request $request)
    {
        return Order::getUserOrders($request->get('id'));
    }
}
