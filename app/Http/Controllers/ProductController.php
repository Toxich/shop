<?php

namespace App\Http\Controllers;

use App\Favourite;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public static function index()
    {
    }

    public static function showProduct(Request $request)
    {
        return Product::getProduct($request->get('id'));
    }

    public function indexList()
    {
        return Product::all();
    }

    public function dateRange(Request $request)
    {
        return Product::rowsByDateRange($request->get('startDate'), $request->get('endDate'));
    }

}
