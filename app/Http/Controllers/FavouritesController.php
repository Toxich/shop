<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Favourite;
use Illuminate\Http\Request;

class FavouritesController extends Controller
{
    public static function getFavourites(Request $request)
    {
        return Favourite::getUserFavourites($request->get('id'));
    }

    public static function addOrDeleteFavourite(Request $request)
    {
        return Favourite::addOrDeleteFavourite($request->get('prodId'), $request->get('userId'));
    }

    public static function deleteFavourite(Request $request)
    {
        return Favourite::deleteFavourite($request->get('id'));
    }
}
