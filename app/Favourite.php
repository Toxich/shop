<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Favourite extends Model
{
    protected $table = 'favourites';
    protected $fillable = [
        'id',
        'user_id',
        'product_id',
    ];

    public static function getUserFavourites($id)
    {
        return DB::table('favourites')
            ->join('products', 'products.id', '=', 'favourites.product_id')
            ->select('title', 'price', 'products.id')
            ->where('user_id', $id)
            ->get();
    }

    public static function addOrDeleteFavourite($productId, $userId)
    {
        $result = Favourite::where('user_id', $userId)->where('product_id', $productId)->first();
        if($result) {
            Favourite::where('user_id', $userId)->where('product_id', $productId)->delete();
            return null;
        }else{
            Favourite::create([
               'user_id' => $userId,
               'product_id' => $productId,
           ]);
           return $productId;
        }
    }

}
