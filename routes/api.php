<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['prefix' => 'auth'],
    function ($router) {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    }
);

// Product
Route::get('products', 'ProductController@indexList');
Route::get('products/date', 'ProductController@dateRange');
Route::get('product/{id}', 'ProductController@index')->where('id', '[0-9]+');
Route::get('product/show-product', 'ProductController@showProduct');


//Favourites
Route::post('favourites/add_favourite', 'FavouritesController@addOrDeleteFavourite');
Route::post('favourites/delete', 'FavouritesController@deleteFavourite');
Route::get('favourites', 'FavouritesController@getFavourites');

// Cart
Route::get('cart', 'CartController@getCart');
Route::post('cart/add_cart', 'CartController@addOrDeleteCart');

//Order
Route::get('orders', 'OrderController@getOrders');

// User page
Route::post('user/update', 'UserController@updateUser');

// route for processing payment
Route::post('paypal', 'PaymentController@payWithpaypal');

// route for check status of the payment
Route::get('status', 'PaymentController@getPaymentStatus');
Route::get('status/getStatus', 'PaymentController@sendPaymentStatus');
