1. Create a folder for project
2. Start docker
3. Open Windows PowerShell
4. Go to your folder in PowerShell
5. Enter: docker network create laravel-docker
   * edit dockerfile and docker-compose.yml if needed
6. Run: docker-compose build
7. Copy project from gitlab
8. Run: composer install
 * Change your path to index.php file in ./docker/default if needed
9. Run: npm init
10. Run: npm install
11. Then: npm install --global cross-env
12. Run: npm run dev
13. Change your .env file according to your settings if needed
14. Run: php artisan migrate