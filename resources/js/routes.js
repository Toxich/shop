
import Home from './components/Home.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import Dashboard from './components/Dashboard.vue';
import ProductsList from './components/ProductsList.vue';
import Userpage from './components/Userpage.vue';
import ProductShow from './components/ProductShow.vue';
import PaymentStatus from './components/PaymentStatus.vue';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
    },
    {
        path: '/products',
        name: 'products',
        component: ProductsList,
    },
    {
        path: '/product/:id',
        name: 'productShow',
        component: ProductShow,
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: '/payment-status',
        name: 'payment-status',
        component: PaymentStatus,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: '/userpage',
        name: 'userpage',
        component: Userpage,
        meta: {
            requiresAuth: true
        },
    }
];
