const puppeteer = require('puppeteer');
const con = require('./dbconnect');

async function getData(url) {
    try {
        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
        const page = await browser.newPage();
        await page.goto(url);
        const data = [];
        //      console.log(await page.evaluate(() => location.href));

        // Collecting links to products on page
        let links = await page.evaluate(() => {
            let elements = Array.from(document.querySelectorAll('.a-link-normal.s-access-detail-page.s-color-twister-title-link.a-text-normal'));
            return elements.map(element => {
                return element.getAttribute('href');
            })
        });
        const texts = [];
        const prices = [];
        const availability = [];
        const image_names = [];
        let image;
        let price;
        let priceString;
        let image_index = 0;
        const database = [];

        con.query('SELECT title FROM products ', function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                database.push(result[i].title);
            }
        });

        con.query('SELECT id FROM products ORDER BY id DESC LIMIT 1', function (err, result) {
            if (err) throw err;
            if (result[0]) {
                console.log(result[0].id);
                image_index = result[0].id;
            }
        });

        // Getting titles, prices and images on every product page


        for (var i = 0; i < links.length; i++) {
            await page.goto(links[i]);
            texts.push(await page.evaluate(() => {
                    return document.querySelector('.a-size-large.a-spacing-none').innerText;
                })
            );

            priceString = await page.evaluate(() => {
                let priceElement = document.querySelector('#priceblock_ourprice');
                if (priceElement != null) {
                    return document.querySelector('#priceblock_ourprice').innerText;
                } else {
                    return '0';
                }
            });
            price = priceString.replace(/[^0-9.]/g, "");
            console.log(price);
            prices.push(parseFloat(price).toFixed(2));

            availability.push(await page.evaluate(() => {
                    return document.querySelector('#availability').innerText;
                })
            );

            image = await page.$('#landingImage');
            image_index++;
            image_names.push('product-image-' + image_index + '.png');
            await image.screenshot({
                path: '../../public/images/product-image-' + image_index + '.png',
                omitBackground: true,
            });
        }
        console.log(database);
        console.log(texts);
        console.log(prices);
        console.log(availability);

        // Saving to database

        for (let i = 0; i < texts.length; i++) {
            var category = 1;
            var titleData = texts[i];
            var availabilityData = availability[i];
            var priceData = prices[i];
            var imageData = image_names[i];
            console.log(titleData);
            console.log(database[1]);
            console.log(database.includes(titleData));

            if (!(database.includes(titleData))) {


                var sql = ('INSERT INTO products (category_id, title, price, image, availability) VALUES ('
                    + ' \' ' + category + ' \' ' + ' , '
                    + ' \' ' + titleData + ' \' ' + ' ,  '
                    + ' \' ' + priceData + ' \' ' + ' ,  '
                    + '\'' + imageData + '\'' + ' ,  '
                    + ' \' ' + availabilityData + ' \' ' + ' ) ');
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted");
                });
            }
        }
        con.end();

        await browser.close();
        return data;
    } catch (e) {
        console.log(e);
    }
}

//const args = require('minimist')(process.argv.slice(2))
var text = process.argv.slice(2)[0];
console.log(text);
const url = 'https://www.amazon.com/b?node=16225007011&pf_rd_r=HH67QP1WG66B3PWZEV4K&pf_rd_p=e5b0c85f-569c-4c90-a58f-0c0a260e45a0';
getData(url)
    .then();
