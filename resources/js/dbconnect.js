const mysql = require('mysql');

const con = mysql.createConnection({
    host: "mysql",
    user: "root",
    password: "root",
    database: "shop"
});

con.connect(function(err) {
    if (err) throw err;
});

module.exports = con;
